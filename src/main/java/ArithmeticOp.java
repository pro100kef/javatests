public class ArithmeticOp {
    public void Test1(){
        int a = 2;
        int b = 5;
        //int res = b * 3 + 20 / 2 * a--; the variable res equals 35.

        int step1 = a--; // step1 = a-- = a = 2.
        int step2 = b * 3;
        int step3 = 20 / 2;
        int step4 = step3 * step1;
        int step5 = step2 + step4;
        System.out.println("res1 = " + step5);
    }

    public void Test2(){
        int num1 = 4;
        int num2 = 5;
        int num3 = 15;
        int num4 = 10;
        int num5 = 5;
        int res = 12;
        //res += num1 * num2 + num3 % num4 / num5; the variable equals 33.

        int step1 = num1 * num2;
        int step2 = num3 % num4;
        int step3 = step2 / num5;
        int step4 = step1 + step3;
        int step5 = res + step4;
        System.out.println("res2 = " + step5);
    }

    public void Test3(){
        int x = 8;
        int y = 9;
        int z = x++ + ++y; // x++ = 8, ++y = 10, z = 18.
        System.out.println("res3 = " + z);
    }

    public void Test5(){
        double x1 = 8.8;
        double y1 = 1.5;
        double z1 = x1 - y1;
        // The Double datatype being the double datatype.
        System.out.println("res5 = " + z1);
    }
}