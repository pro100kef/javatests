public class Loops {
    public void Test1() {
        int i = 5;
        int k = 0;
        while (i > 0) {
            k++;
            i *= 3;
            i *= -2;
        }
        /* Loop will execute 1 time because the variable i
        will be equal -30 after the first iteration. */
        System.out.println("res1 = " + k);
    }

    public void Test2() {
        int j = 2;
        int k = 0;
        for (int i = 1; i < 100; i = i + 2) {
            k++;
            j = j - 1;
            while (j < 15) {
                j = j + 5;
            }
        }
        /* The expression j = j - 1 will execute 50 times
           because of the for loop condition (100 / 2 = 50). */
        System.out.println("res2 = " + k);
    }

    public void Test3() {
        int j = 2;
        int k = 0;
        for (int i = 2; i < 32; i = i * 2) {
            while (i < j) {
                j = j * 2;
            }
            k++;
            i = j - i;
            if (k >= 228) break;
        }
        /* It's a vicious cycle, t.i. an infinity loop.
           Firstly, the loop "while" will make the variable j a large negative number.
           Then the variable i will become like that too and will multiply itself
           by 2 with each iteration, which will make the "for" loop infinite. */
        System.out.println("res3 = " + k);
    }

    public void Test4() {
        for (int i = 1; i < 3; i++) {
            switch (i) {
                default:
                    System.out.printf("res4 = %d \n", i++);
                    break;
            }
        }
        /* output: res4 = 1, because the variable i = 3
        before the 2nd iteration => the condition (i < 3) won't be met. */
    }
}