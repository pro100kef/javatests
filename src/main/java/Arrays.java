public class Arrays {
    public void Test2() {
        int[][] numbers =
                {
                        {1, 2},
                        {3, 4},
                        {5, 6}
                };

        // System.out.println((numbers[3][2]));
        /* Compilation error because of indexes that are
           out of reach, first array index is 0. */
        System.out.println("res2 = compilation error");
    }

    public void Test4() {
        // int[][] numbers = new int[3][3]{ {1, 2, 3}, {3, 4, 5}, {6, 7, 8}};
        //System.out.println(numbers[2][1]);

        /* Compilation error. In this situation, new int[3][3] is wrong syntax.
           new int[][] would be right though. */
        System.out.println("res4 = compilation error");
    }
}