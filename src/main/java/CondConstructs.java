public class CondConstructs {
    public void Test1(){
        int a = 5;
        if (a <= 0) if (a ==5) a++; else a--;

        /* The condition (a <= 0) always false =>
           the variable a will remain equal 5. */
        System.out.println("res1 = " + a);
    }

    public void Test2(){
        int a = 5;
        switch(a) {
            case 4:
                a++;
            case 5:
                a++;
            case 6:
            case 7:
            case 8:
                a++;
                break;
            case 9:
                a++;
                break;
            default:
                a++;
        }
        // case 5: a++ => case 6: => default: a++ => a = 7.
        System.out.println("res2 = " + a);
    }
}