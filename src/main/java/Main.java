public class Main {
    public static void main(String[] args){
        // Arithmetic Operations Tests.
        System.out.println("Arithmetic Operations Tests:");
        ArithmeticOp arithmeticOp = new ArithmeticOp();
        arithmeticOp.Test1();
        arithmeticOp.Test2();
        arithmeticOp.Test3();
        arithmeticOp.Test5();
        System.out.println();

        // Primitive Types Converting Test.
        System.out.println("Primitive Types Converting Test:");
        PrimTypesConvert primTypesConvert = new PrimTypesConvert();
        primTypesConvert.Test2();
        System.out.println();

        // Conditional Constructs Tests.
        System.out.println("Conditional Constructs Tests:");
        CondConstructs condConstructs = new CondConstructs();
        condConstructs.Test1();
        condConstructs.Test2();
        System.out.println();

        // Loops Tests.
        System.out.println("Loops Tests:");
        Loops loops = new Loops();
        loops.Test1();
        loops.Test2();
        loops.Test3();
        loops.Test4();
        System.out.println();

        // Arrays Tests.
        System.out.println("Arrays Tests:");
        Arrays arrays = new Arrays();
        arrays.Test2();
        arrays.Test4();
    }
}