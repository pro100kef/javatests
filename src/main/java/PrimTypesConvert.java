public class PrimTypesConvert {
    public void Test2() {
        short shortNum = 257;
        byte byteNum = (byte)shortNum;

        /* the variable bytenum equals 1 because after the
           narrowing only 8 right bits left (byte). */
        System.out.println("res2 = " + byteNum);
    }
}